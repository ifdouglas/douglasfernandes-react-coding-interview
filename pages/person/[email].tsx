import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message, Form } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company, Person } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const [isEditMode, setIsEditMode] = useState<boolean>(false)
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onFinish = (values: Person) => {
    save({ ...data, ...values }).then(() => setIsEditMode(false))
  }

  const onFinishFailed = (error: any) => {
    console.error('error', error)
  }

  const EditForm = () => (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ ...data }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item label="Name" name="name">
        <Input defaultValue={data.name} value={data.name} />
      </Form.Item>
      <Form.Item label="Gender" name="gender">
        <Input defaultValue={data.gender} value={data.gender} />
      </Form.Item>
      <Form.Item label="Phone" name="phone">
        <Input defaultValue={data.phone} value={data.phone} />
      </Form.Item>
      <Form.Item label="Birthday" name="birthday">
        <Input defaultValue={data.birthday} value={data.birthday} />
      </Form.Item>
      <Button onClick={() => setIsEditMode(false)}>
        Cancel
      </Button>
      <Button type="primary" htmlType="submit">
        Submit
      </Button>
    </Form>
  )

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setIsEditMode(true)}>
            Edit
          </Button>,
        ]}
      >
        {data && !isEditMode && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        )}
        {isEditMode && <EditForm />}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => { }}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
